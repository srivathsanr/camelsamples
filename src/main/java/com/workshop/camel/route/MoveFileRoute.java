package com.workshop.camel.route;

import org.apache.camel.builder.RouteBuilder;

public class MoveFileRoute extends RouteBuilder {

	public void configure() throws Exception {
        from("file://C:/camelsource")
        .to("log://org.apache.camel.howto?showAll=true")
        .to("file://C:/cameltarget");
	}
	
}
