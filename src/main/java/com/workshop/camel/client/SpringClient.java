package com.workshop.camel.client;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringClient {

	public static void main(String[] args) throws Exception {
	    AbstractApplicationContext springContext = new ClassPathXmlApplicationContext("spring-camel-routes.xml");
	    springContext.start();
	    Thread.sleep(50000);
	    springContext.stop();
	}

}
